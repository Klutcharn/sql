﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using sql_project.Repositories;
using sql_project.Models;

namespace sql_project.Methods
{
    public class GenresMethods
    {


        public static void SelectTopGenres(IGenresRepository repository)
        {
            //calls the function to get all the top genres per customer and then calls the printer function to print out all the results from the list.
            PrintTopGenres(repository.GetAllGenres());

        }
        public static void PrintGenres(Genres genres)
            //Prints Genre objects with formatting
        {
            Console.WriteLine("{0,-4} {1,-10} {2,-20} {3, -4} ", genres.CustomerId, genres.CustomerFirstName, genres.GenreName, genres.GenreCount);
        }
        public static void PrintTopGenres(List<Genres> genresList)
            //Lists the top genre per customer from a sorted list of invoices
        {
            List<Genres> topList = new List<Genres>();
            Genres tiedElem;
            Genres currentHighestElem;
            for (int i = 0; i < genresList.Count-1;i++)
            {
                currentHighestElem = genresList[i];
                tiedElem = genresList[i+1];
                bool tied = false;
                try
                {
                    do
                    {
                        i++;
                        if (genresList[i].GenreCount > currentHighestElem.GenreCount)
                        {
                            currentHighestElem = genresList[i];

                        }else if (genresList[i].GenreCount == currentHighestElem.GenreCount)
                        {
                            tied = true;

                            tiedElem = genresList[i];
                        }
                    } while (genresList[i].CustomerId == genresList[i + 1].CustomerId);
                }catch (Exception ex) { 
                }
              
                topList.Add(currentHighestElem);
                if (tied)
                {
                    if (currentHighestElem.GenreCount == tiedElem.GenreCount)
                    {
                        topList.Add(tiedElem);
                    }
                }
            }
            foreach (Genres genres in topList)
            {
                PrintGenres(genres);
            }
        }
        public static void PrintTopGenreById(List<Genres> genresList)
            //Calls the print function
        {
           
            foreach(Genres genre in genresList)
            {
                PrintGenres(genre);
            }

        }

        public static void SelectTopGenresById(IGenresRepository repository, int id)
        //Invoker
        //Call the function to get the top genre with ties per customer id and then calls the printer to print out everything to the console.
        {
            PrintTopGenres(repository.GetGenresById(id));

        }

    }
}
