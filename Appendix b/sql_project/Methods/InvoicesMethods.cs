﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using sql_project.Repositories;
using sql_project.Models;

namespace sql_project.Methods
{
    public class InvoicesMethods
    {


        public static void SelectTopInvoiceTotal(IInvoiceRepository repository)
            // Invoker
        {
            PrintInvoicesList(repository.GetInvoiceTotal());

        }
        public static void PrintInvoices(Invoices invoices)
            // Prints with formatting
        {
            Console.WriteLine("{0,-15} {1,-15} {2,-5} ", invoices.CustomerFirstName, invoices.CustomerLastName, invoices.InvoiceTotal);
        }
        public static void PrintInvoicesList(IEnumerable<Invoices> invoicesList)
            // Calls the print function for each element in a list
        {
            foreach (Invoices invoices in invoicesList)
            {
                PrintInvoices(invoices);

            }
        }
    }
}
