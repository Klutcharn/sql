﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using sql_project.Repositories;
using sql_project.Models;
using sql_project.Methods;

namespace sql_project.Methods
{
    public class CustomerMethods
    {

        public static void SelectAll(ICustomerRepository repository)
        {
            //calls the printer method to print all customer
            Methods.PrintCustomers(repository.GetAllCustomers());

        }

        public static void SelectId(ICustomerRepository repository, int id)
        {
            //calls the printer method to print the selected customer
            Methods.PrintCustomer(repository.GetCustomerId(id));
        }

        public static void SelectName(ICustomerRepository repository, string name)
        {
            //calls the printer method to print the selected customer
            Methods.PrintCustomer(repository.GetCustomerName(name));
        }

      
        public static void PrintPage(ICustomerRepository repository, int limit, int offset)
        {
            //calls the printer method to print the all the selected customers
            Methods.PrintCustomers(repository.GetCustomerPage(limit, offset));

        }
        public static void CreateCustomer(ICustomerRepository repository,string firstname,string lastname, string email, string country, string postalcode, string phone)
        {
            //calls the add customer function
            repository.AddNewCustomer(firstname, lastname, email, country, postalcode,phone);
        }
        public static void UpdateCustomer(ICustomerRepository repository, int id , string updatedLastname)
        {
            repository.UpdateCustomer(id, updatedLastname);
        }


    }
}
