﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using sql_project.Repositories;
using sql_project.Models;


namespace sql_project.Methods
{
    public class CountryMethods
    {
        public static void PrintCountries(IEnumerable<Country> countries)
            // Calls the print function
        {
            foreach (Country country in countries)
            {
                PrintCountry(country);

            }
        }

        public static void PrintCountry(Country country)
            // Prints with formatting
        {
            Console.WriteLine("{0,-6} {1,-25}", country.CountryCounter, country.CountryName);
        }
        public static void SelectCountries(ICountryRepository repository)
        {
            //selects all countries and call the printer function on that list that it returns
            PrintCountries(repository.GetAllCountries());

        }
    }
}
