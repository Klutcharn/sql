﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using sql_project.Repositories;
using sql_project.Models;

namespace sql_project.Methods
{
    public class Methods
    {
        public static void PrintCustomers(IEnumerable<Customer> customers)
            // Calls print function
        {
            foreach (Customer customer in customers)
            {
                PrintCustomer(customer);

            }
        }

        public static void PrintCustomer(Customer customer)
            // Prints customer with formatting
        {

            Console.WriteLine("{0,3} {1,-10} {2,-15} {3,-30} {4,-15} {5,-15} {6,-20} ", customer.CustomerID, customer.CustomerFirstName, customer.CustomerLastName, customer.CustomerEmail, customer.CustomerCountry, customer.CustomerPostalCode, customer.CustomerPhoneNumber);
             }

        public static void PrintPage(ICustomerRepository repository)
            //Invoker
        {
            PrintCustomers(repository.GetCustomerPage(5, 10));

        }

    }
}
