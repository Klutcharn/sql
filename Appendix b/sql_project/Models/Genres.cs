﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sql_project.Models
{
    public class Genres
        // Creates a "genres object", which counts the invoiced tracks per genre for each customer
    {
        public int CustomerId { get; set; }
        public string CustomerFirstName { get; set; }
        public string GenreName { get; set; }
        public int GenreCount { get; set; }
    }

}
