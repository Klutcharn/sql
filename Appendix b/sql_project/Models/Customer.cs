﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sql_project.Models
{
    public class Customer
        // Creates a customer object
    {
        public int CustomerID { get; set; }
        public string CustomerFirstName { get; set; }
        public string CustomerLastName { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerCountry { get; set; }
        public string CustomerPhoneNumber { get; set; }
        public string CustomerPostalCode { get; set; }

    }

}
