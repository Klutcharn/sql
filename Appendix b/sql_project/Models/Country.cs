﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sql_project.Models
{
    public class Country
        //Creates a country object
    {
        public string CountryName { get; set; }

        public int CountryCounter { get ; set; }
        
    }

}
