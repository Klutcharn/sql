﻿using sql_project.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sql_project.Repositories
{
    public interface IInvoiceRepository
    // Repository interface for invoices
    {
        public List<Invoices> GetInvoiceTotal();

    }
}
