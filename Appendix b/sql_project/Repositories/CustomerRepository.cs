﻿using Microsoft.Data.SqlClient;
using sql_project.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sql_project.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        public List<Customer> GetAllCustomers()
        {
            ///<summary>
            ///This function gets all the customer with id,first name, last name, email, country, postal code and phone number.
            ///it will add every customer to a list that we then can print out with a printer function
            ///</summary>
            List<Customer> custList = new List<Customer>();
            string sql = "SELECT CustomerId,FirstName,LastName,Email,Country,PostalCode,Phone from Customer";
            try
            {

                //connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    //make a command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    { // reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                //handle result
                                Customer temp = new Customer();
                                temp.CustomerID = reader.GetInt32(0);
                                temp.CustomerFirstName = reader.GetString(1);
                                temp.CustomerLastName = reader.GetString(2);
                                temp.CustomerEmail = reader.GetString(3);
                                temp.CustomerCountry = reader.GetString(4);
                                if (!reader.IsDBNull(5))
                                {
                                    temp.CustomerPostalCode = reader.GetString(5);
                                }
                                else
                                {
                                    temp.CustomerPostalCode = "None";
                                }
                                if (!reader.IsDBNull(6))
                                {
                                    temp.CustomerPhoneNumber = reader.GetString(6);
                                }
                                else
                                {
                                    temp.CustomerPhoneNumber = "None";
                                }


                                custList.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            //returning list of all customers
            return custList;

        }

        public Customer GetCustomerId(int id)
        {
            ///<summary>
            ///This function gets a customer with id,first name, last name, email, country, postal code and phone number based on the customerId.
            ///it will add every customer to a customer object that we then can print to console or work with.
            ///</summary>
            Customer cust = new Customer();
            string sql = $"SELECT CustomerId,FirstName,LastName,Email,Country,PostalCode,Phone from Customer where CustomerId = {id}";
            try
            {

                //connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    //make a command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    { // reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                //handle result
                                Customer temp = new Customer();
                                temp.CustomerID = reader.GetInt32(0);
                                temp.CustomerFirstName = reader.GetString(1);
                                temp.CustomerLastName = reader.GetString(2);
                                temp.CustomerEmail = reader.GetString(3);
                                temp.CustomerCountry = reader.GetString(4);
                                if (!reader.IsDBNull(5))
                                {
                                    temp.CustomerPostalCode = reader.GetString(5);
                                }
                                else
                                {
                                    temp.CustomerPostalCode = "None";
                                }
                                if (!reader.IsDBNull(6))
                                {
                                    temp.CustomerPhoneNumber = reader.GetString(6);
                                }
                                else
                                {
                                    temp.CustomerPhoneNumber = "None";
                                }
                                cust = temp;
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            //returning list of all customers
            return cust;
        }

        public Customer GetCustomerName(string name)
        {
            ///<summary>
            ///This function gets a customer with id,first name, last name, email, country, postal code and phone number based on the customers first name.
            ///it will add every customer to a customer object that we then can print to console or work with.
            ///</summary>
            Customer cust = new Customer();
            string sql = $"SELECT CustomerId,FirstName,LastName,Email,Country,PostalCode,Phone from Customer where FirstName Like '%{name}%'";
            try
            {

                //connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    //make a command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    { // reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                //handle result
                                Customer temp = new Customer();
                                temp.CustomerID = reader.GetInt32(0);
                                temp.CustomerFirstName = reader.GetString(1);
                                temp.CustomerLastName = reader.GetString(2);
                                temp.CustomerEmail = reader.GetString(3);
                                temp.CustomerCountry = reader.GetString(4);
                                if (!reader.IsDBNull(5))
                                {
                                    temp.CustomerPostalCode = reader.GetString(5);
                                }
                                else
                                {
                                    temp.CustomerPostalCode = "None";
                                }
                                if (!reader.IsDBNull(6))
                                {
                                    temp.CustomerPhoneNumber = reader.GetString(6);
                                }
                                else
                                {
                                    temp.CustomerPhoneNumber = "None";
                                }
                                cust = temp;
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            //returning list of all customers
            return cust;
        }

        public List<Customer> GetCustomerPage(int limit, int offset)
        {
            ///<summary>
            ///This function gets all customer with id,first name, last name, email, country, postal code and phone number.
            ///It will only print customers within the limit and offset to return a page.
            ///it will add every customer to a customer list that we then can print to console or work with.
            ///</summary>
            List<Customer> custList = new List<Customer>();

            string sql = $"SELECT CustomerId,FirstName,LastName,Email,Country,PostalCode,Phone from Customer order by CustomerId asc OFFSET {offset} rows FETCH NEXT {limit} ROWS ONLY;";
            try
            {

                //connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    //make a command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    { // reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                //handle result
                                Customer temp = new Customer();
                                temp.CustomerID = reader.GetInt32(0);
                                temp.CustomerFirstName = reader.GetString(1);
                                temp.CustomerLastName = reader.GetString(2);
                                temp.CustomerEmail = reader.GetString(3);
                                temp.CustomerCountry = reader.GetString(4);
                                if (!reader.IsDBNull(5))
                                {
                                    temp.CustomerPostalCode = reader.GetString(5);
                                }
                                else
                                {
                                    temp.CustomerPostalCode = "None";
                                }
                                if (!reader.IsDBNull(6))
                                {
                                    temp.CustomerPhoneNumber = reader.GetString(6);
                                }
                                else
                                {
                                    temp.CustomerPhoneNumber = "None";
                                }
                                custList.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            //returning list of all customers
            return custList;
        }


        public bool AddNewCustomer(string firstName, string lastName, string email, string country, string postalCode, string phone)
        {
            ///<summary>
            ///This function adds a customer with id,first name, last name, email, country, postal code and phone number
            ///it will add a customer to the database based on the input.
            ///</summary>
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    //open connection
                    connection.Open();
                    //make sql string
                    string sql = "INSERT INTO Customer (FirstName,LastName,Email,Country,PostalCode,Phone) VALUES (@FirstName,@LastName,@Email,@Country,@PostalCode,@Phone)";
                    //make a command
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        //Handle action, adding customer to db.
                        command.Parameters.AddWithValue("@FirstName", firstName);
                        command.Parameters.AddWithValue("@LastName", lastName);
                        command.Parameters.AddWithValue("@Email", email);
                        command.Parameters.AddWithValue("@Country", country);
                        command.Parameters.AddWithValue("@PostalCode", postalCode);
                        command.Parameters.AddWithValue("@Phone", phone);
                        command.ExecuteNonQuery();

                    }

                }

            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }

            return true;
        }
        public bool UpdateCustomer(int id, string updatedLastName)
        {
            ///<summary>
            ///This function Update a customers lastname based on the id input.
            ///</summary>
            try
            {
                string sql = "UPDATE Customer SET LastName = @newLastName WHERE CustomerId = @customerId";

                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@newLastName",updatedLastName);
                        command.Parameters.AddWithValue("@customerId", id);

                        command.ExecuteNonQuery();
                    }
                }

            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
            return true;
        }



    }
}
