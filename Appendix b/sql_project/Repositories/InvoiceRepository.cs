﻿using Microsoft.Data.SqlClient;
using sql_project.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sql_project.Repositories
{
    public class InvoiceRepository : IInvoiceRepository
    // Generates a list of created invoice objects from sql command
    {
        public List<Invoices> GetInvoiceTotal()
        {
            string sql = "SELECT  Invoice.CustomerId, Customer.FirstName, Customer.LastName, SUM (Total)  FROM Invoice INNER JOIN Customer ON Invoice.CustomerId = Customer.CustomerId GROUP BY Invoice.CustomerId , Customer.FirstName, Customer.LastName ORDER BY SUM(Total) DESC ";
          

            List<Invoices> invoicesList = new List<Invoices>();
            try
            {

                //connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    //make a command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    { // reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                //handle result
                                Invoices temp = new Invoices();
                                temp.CustomerId = reader.GetInt32(0);
                                temp.CustomerFirstName = reader.GetString(1);
                                temp.CustomerLastName = reader.GetString(2);
                                temp.InvoiceTotal = reader.GetDecimal(3);
                                
                                invoicesList.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            //returning list of all customers
            return invoicesList;
        }

    }
}
