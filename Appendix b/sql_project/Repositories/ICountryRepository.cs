﻿using sql_project.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sql_project.Repositories
{
    public interface ICountryRepository
        // Repository interface for country
    {
        public List<Country> GetAllCountries();

    }
}
