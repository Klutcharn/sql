﻿using sql_project.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sql_project.Repositories
{
    public interface ICustomerRepository
    // Repository interface for customer
    {
        public Customer GetCustomerId(int id);
        public Customer GetCustomerName(String name);
        public List<Customer> GetAllCustomers();
        public List<Customer> GetCustomerPage(int limit, int offset);
        public bool AddNewCustomer(string firstName, string lastName, string email, string country, string postalCode, string phone);
        public bool UpdateCustomer(int id, string updatedLastname);

    }
}
