﻿using sql_project.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sql_project.Repositories
{
    public interface IGenresRepository
    // Repository interface for genres
    {
        public List<Genres> GetAllGenres();

        public List<Genres> GetGenresById(int id);

    }
}
