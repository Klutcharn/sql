﻿using Microsoft.Data.SqlClient;
using sql_project.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sql_project.Repositories
{
    public class CountryRepository : ICountryRepository
    {
        // Generates a list of created country objects from sql command
        public List<Country> GetAllCountries()
        {
            string sql = "SELECT COUNT (Country), Country FROM Customer GROUP BY Country ORDER BY COUNT(Country) DESC";
            List<Country> countryList = new List<Country>();
            try
            {
                //connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    //make a command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    { // reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                //handle result
                                Country temp = new Country();
                                temp.CountryCounter = reader.GetInt32(0);
                                temp.CountryName = reader.GetString(1);

                                countryList.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            //returning list of all customers
            return countryList;
        }

    }
}
