﻿using Microsoft.Data.SqlClient;
using sql_project.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sql_project.Repositories
{
    public class GenresRepository : IGenresRepository
    {
        public List<Genres> GetAllGenres()
        // Generates a list of created genre objects from sql command
        {
            string sql = "SELECT t.id AS cid, t.Fname AS Customer_name, t.Gname AS Genre_name, max(t.Genrecount) AS Genre FROM( " +
                "SELECT Customer.CustomerId AS id, Customer.FirstName AS Fname, genre.Name AS Gname, COUNT(genre.Name) AS Genrecount FROM InvoiceLine " +
                "INNER JOIN Invoice ON InvoiceLine.InvoiceId = Invoice.InvoiceId " +
                "INNER JOIN Track ON InvoiceLine.TrackId = Track.TrackId " +
                "INNER JOIN Genre ON Track.GenreId = Genre.GenreId " +
                "INNER JOIN Customer ON Invoice.CustomerId = Customer.CustomerId " +
                "GROUP BY Customer.CustomerId, Customer.FirstName, Genre.Name) AS t  GROUP BY t.id , t.Fname, t.Gname order by t.id";
          

            List<Genres> genresList = new List<Genres>();
            try
            {

                //connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    //make a command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    { // reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                //handle result
                                Genres temp = new Genres();
                                temp.CustomerId = reader.GetInt32(0);
                                temp.CustomerFirstName = reader.GetString(1);
                                temp.GenreName = reader.GetString(2);
                                temp.GenreCount = reader.GetInt32(3);
                                genresList.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            //returning list of all customers
            return genresList;
        }

        public List<Genres> GetGenresById(int id)
        {
            // Generates an object of genre from sql command based on the id
            string sql = "select top 1 with ties Customer.CustomerId,Customer.FirstName, genre.name, count(genre.name) as total from Customer "+
            "inner join Invoice on Invoice.CustomerId = Customer.CustomerId " +
            "inner join InvoiceLine on Invoice.InvoiceId = InvoiceLine.InvoiceId " +
            "inner join Track on invoiceLine.TrackId = Track.TrackId " +
            "inner join Genre on Track.GenreId = Genre.GenreId " +
            $"where Customer.CustomerId = {id} " +
            "group by Customer.FirstName, genre.name, Customer.CustomerId " +
            "order by total desc; ";
            List<Genres> genresList = new List<Genres>();
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    //make a command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    { // reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                //handle result
                                Genres temp = new Genres();
                                temp.CustomerId = reader.GetInt32(0);
                                temp.CustomerFirstName = reader.GetString(1);
                                temp.GenreName = reader.GetString(2);
                                temp.GenreCount = reader.GetInt32(3);
                                genresList.Add(temp);
                            }
                        }
                    }
                }
            }catch(SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return genresList;
        }

    }
}
