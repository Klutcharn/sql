﻿// See https://aka.ms/new-console-template for more information
using sql_project.Models;
using sql_project.Repositories;
using sql_project.Methods;

/// <summary>
/// The first section is everything based on the customer, such as:
/// getting all customers
/// getting customer per id
/// getting customer per name
/// getting a page of custonmers
/// add a new customer to the database
/// Prints out a formatted version of the result.
/// </summary>

ICustomerRepository customerRepository = new CustomerRepository();
Console.ForegroundColor = ConsoleColor.Green;
Console.WriteLine("----------------------------------Print All Customers-------------------------------------");
Console.ForegroundColor = ConsoleColor.DarkGreen;
Console.WriteLine("{0,3} {1,-10} {2,-15} {3,-30} {4,-15} {5,-15} {6,-20} ", "Id", "First Name", "Last Name", "Email", "Country", "Postal Code", "Phone Number");
Console.ForegroundColor = ConsoleColor.White;
CustomerMethods.SelectAll(customerRepository);

Console.WriteLine("");
Console.ForegroundColor = ConsoleColor.Green;
Console.WriteLine("------------------------Print Customer From Id------------------------------");
Console.ForegroundColor = ConsoleColor.DarkGreen;
Console.WriteLine("{0,3} {1,-10} {2,-15} {3,-30} {4,-15} {5,-15} {6,-20} ", "Id", "First Name", "Last Name", "Email", "Country", "Postal Code", "Phone Number");
Console.ForegroundColor = ConsoleColor.White;
CustomerMethods.SelectId(customerRepository, id: 1);

Console.WriteLine("");
Console.ForegroundColor = ConsoleColor.Green;
Console.WriteLine("------------------------Print Customer From Name----------------------------");
Console.ForegroundColor = ConsoleColor.DarkGreen;
Console.WriteLine("{0,3} {1,-10} {2,-15} {3,-30} {4,-15} {5,-15} {6,-20} ", "Id", "First Name", "Last Name", "Email", "Country", "Postal Code", "Phone Number");
Console.ForegroundColor = ConsoleColor.White;
CustomerMethods.SelectName(customerRepository, name:"Tim");
//
Console.WriteLine("");
Console.ForegroundColor = ConsoleColor.Green;
Console.WriteLine("-----------------------Print A Page Of Customers-----------------------");
Console.ForegroundColor = ConsoleColor.DarkGreen;
Console.WriteLine("{0,3} {1,-10} {2,-15} {3,-30} {4,-15} {5,-15} {6,-20} ", "Id", "First Name", "Last Name", "Email", "Country", "Postal Code", "Phone Number");
Console.ForegroundColor = ConsoleColor.White;
CustomerMethods.PrintPage(customerRepository, limit: 5, offset:10);

//Update a customer
CustomerMethods.UpdateCustomer(customerRepository, id: 12, updatedLastname:"Persson");
//Add a new customer
CustomerMethods.CreateCustomer(customerRepository, firstname:"Joachim",lastname:"Thomasson",email:"asdasd@emaill.com",country:"Sweden",postalcode:"61434",phone:"0123456789");

/// <summary>
/// The next section is about the country side, here we call the function to get amount of customers per country.
/// Prints out a formatted version of the result.
/// </summary>
ICountryRepository countryRepository = new CountryRepository();
Console.WriteLine("");
Console.ForegroundColor = ConsoleColor.Green;
Console.WriteLine("-----------------------Print Amount of Customers Per Country-----------------");
Console.ForegroundColor = ConsoleColor.DarkGreen;
Console.WriteLine("{0,-6} {1,-25}", "Amount", "Country");
Console.ForegroundColor = ConsoleColor.White;
CountryMethods.SelectCountries(countryRepository);

/// <summary>
/// The next section is about the invoice per customer, here we print out the total amount of invoice total per customer.
/// Prints out a formatted version of the result.
/// </summary>
IInvoiceRepository invoiceRepository = new InvoiceRepository();
Console.WriteLine("");
Console.ForegroundColor = ConsoleColor.Green;
Console.WriteLine("-----------------------Print Invoice Total Per Customer----------------------------");
Console.ForegroundColor = ConsoleColor.DarkGreen;
Console.WriteLine("{0,-15} {1,-15} {2,-5} ", "First Name", "Last Name", "Total Amount");
Console.ForegroundColor = ConsoleColor.White;
InvoicesMethods.SelectTopInvoiceTotal(invoiceRepository);

/// <summary>
/// The next section is about the genres, here we print out the most popular genre with ties based in the id.
/// and the last function prints out a list of all customers with their respective most popular genre with ties(this is an extra just because we wanted to test using c# logic aswell as SQL logic)
/// Prints out a formatted version of the result.
/// </summary>
IGenresRepository genreRepository = new GenresRepository();
Console.WriteLine("");
Console.ForegroundColor = ConsoleColor.Green;
Console.WriteLine("-----------------------Print Most Popular Genre Per Customer By Id----------------------------");
Console.ForegroundColor = ConsoleColor.DarkGreen;
Console.WriteLine("{0,-4} {1,-10} {2,-20} {3, -4} ", "Id", "Name", "Genre", "Amount");
Console.ForegroundColor = ConsoleColor.White;
GenresMethods.SelectTopGenresById(genreRepository, id:12);

Console.WriteLine("");
Console.ForegroundColor = ConsoleColor.Green;
Console.WriteLine("-----------------------Print Most Popular Genre Per Customer----------------------------");
Console.ForegroundColor = ConsoleColor.DarkGreen;
Console.WriteLine("{0,-4} {1,-10} {2,-20} {3, -4} ", "Id", "Name", "Genre", "Amount");
Console.ForegroundColor = ConsoleColor.White;
GenresMethods.SelectTopGenres(genreRepository);




