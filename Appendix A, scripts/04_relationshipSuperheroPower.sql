CREATE TABLE [dbo].[SuperheroPower](
[SuperheroId] int NOT NULL,
[PowerId] int NOT NULL,
FOREIGN KEY (SuperheroId) REFERENCES Superhero(SuperheroId),
FOREIGN KEY (PowerId) REFERENCES Power(PowerId),
PRIMARY KEY (SuperheroId,PowerId)
);