INSERT INTO [dbo].[Power] ([PowerName], [PowerDescription] ) VALUES (N'Flying',N'Flying like a wasp in the ocean');
INSERT INTO [dbo].[Power] ([PowerName], [PowerDescription] ) VALUES (N'Swimming',N'Swimming like a fish in the sky');
INSERT INTO [dbo].[Power] ([PowerName], [PowerDescription] ) VALUES (N'X-ray',N'Looking at babes through objects ;)');
INSERT INTO [dbo].[Power] ([PowerName], [PowerDescription] ) VALUES (N'Exo-skeleton',N'Mega stronk bones and heavy from drinking oat milk');

INSERT INTO [dbo].[SuperheroPower] ([SuperheroID], [PowerId]) VALUES ((select SuperheroId from Superhero where SuperheroName ='Jocke'),(select PowerId from Power where PowerName ='Flying'));
INSERT INTO [dbo].[SuperheroPower] ([SuperheroID], [PowerId]) VALUES ((select SuperheroId from Superhero where SuperheroName ='Jocke'),(select PowerId from Power where PowerName ='X-ray'));
INSERT INTO [dbo].[SuperheroPower] ([SuperheroID], [PowerId]) VALUES ((select SuperheroId from Superhero where SuperheroName ='Thomas'),(select PowerId from Power where PowerName ='Flying'));
INSERT INTO [dbo].[SuperheroPower] ([SuperheroID], [PowerId]) VALUES ((select SuperheroId from Superhero where SuperheroName ='Thomas'),(select PowerId from Power where PowerName ='Exo-skeleton'));
INSERT INTO [dbo].[SuperheroPower] ([SuperheroID], [PowerId]) VALUES ((select SuperheroId from Superhero where SuperheroName ='Mike'),(select PowerId from Power where PowerName ='Swimming'));
INSERT INTO [dbo].[SuperheroPower] ([SuperheroID], [PowerId]) VALUES ((select SuperheroId from Superhero where SuperheroName ='Mike'),(select PowerId from Power where PowerName ='X-ray'));

