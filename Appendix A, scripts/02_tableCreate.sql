
CREATE TABLE [dbo].[Assistant]
(
    [AssistantId] INT NOT NULL IDENTITY ,
    [AssistantName] NVARCHAR(160) NOT NULL,
	PRIMARY KEY (AssistantId)
);
GO
CREATE TABLE [dbo].[Superhero]
(
    [SuperheroId] INT NOT NULL IDENTITY,
    [SuperheroName] NVARCHAR(120),
	[SuperheroAlias] NVARCHAR(120),
	[SuperheroOrigin] NVARCHAR(120),
	PRIMARY KEY (SuperheroId)
);
GO
CREATE TABLE [dbo].[Power]
(
    [PowerId] INT NOT NULL IDENTITY,
    [PowerName] NVARCHAR(120),
	[PowerDescription] NVARCHAR(120),
	PRIMARY KEY (PowerId)
);


